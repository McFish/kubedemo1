const Redis = require('ioredis');
const { Client } = require('@elastic/elasticsearch');

const Express = require('express');
const BodyParser = require('body-parser');

const redisClient = new Redis(6379, "redis");

let redisConnected = undefined;
let elasticConnected = undefined;

// :::::::::::::::::::::::::::::::::::::: //
// Redis
// :::::::::::::::::::::::::::::::::::::: //
redisClient.on('error', (error) => {
    redisConnected = false;
});

redisClient.on('connect', () => {
    redisConnected = true;
});

// :::::::::::::::::::::::::::::::::::::: //
// Elastic
// :::::::::::::::::::::::::::::::::::::: //
const elasticClient = new Client({ node: 'http://elastic:9200' })


// :::::::::::::::::::::::::::::::::::::: //
//
// :::::::::::::::::::::::::::::::::::::: //
const app = Express();

app.use(BodyParser.json());

app.get('/search/:type', async (req, res) => {
  const { type } = req.params;
  if(type !== 'event' && type !== 'topic') {
    return res.status(400).send({ error: 'invalid type' });
  }

  const query = req.query['q'];
  console.log(`Trying to run query: ${query}`);

  try {
    const rsp = await elasticClient.search({
      index: `kubedemo-${type}`,
      body: {
        size: 100,
        from: 0,
        query: {
          "multi_match": {
            query,
            fields: ['title^3', 'body']
          }
        }
      }
    });
    res.send(rsp);
  } catch(err) {
    console.log(err);
    return res.status(500).send({ error: 'elastic failure' });
  }
});

app.get('/status', (req, res) => {
  res.send(`Redis connected: ${redisConnected} ; Elastic connected: ${elasticConnected}`);
});

app.post('/index/:type', async (req, res) => {
  const {title, body} = req.body;
  if(!title || !body) {
    return res.status(400).send({ error: 'param missing' });
  }

  const { type } = req.params;
  if(type !== 'event' && type !== 'topic') {
    return res.status(400).send({ error: 'invalid type' });
  }

  try {
    await elasticClient.index({
      index: `kubedemo-${type}`,
      body: {
        title,
        body
      }
    });
  } catch(err) {
    console.log(err);
    return res.status(500).send({ error: 'elastic failure' });
  }

  console.log(typeof(req.body));
  res.send('OK');
});

app.listen(3000, () => {
  console.log('Application up');
});