FROM node:lts-slim
WORKDIR /service
COPY index.js package*.json ./
RUN npm ci
ENTRYPOINT ["node", "index.js"]
